# IOT - room control and securisation



## Project description


You can find a technical schema of the projec, a diagram of the scripts logic and a comprehensive description of the project in the "Project description" folder


Here is a list of elements put in place with the aim of securing a room and the belongings within it:

1. A dual authentication system requiring knowledge of a password (login is done through a website form) and possession of an RFID badge. The response to the authentication attempt will be displayed on an LCD screen. Pressing a button allows for deauthentication as well.

2. An angle sensor to be placed on a door to detect when it is opened. Additionally, a sound sensor (with sensitivity previously calibrated) is used to determine if there is activity in the room. Both of these elements are connected to an ESP8266.

3. A "pressure sensor" (in this project, it is a button) and a distance sensor, which provide dual security for a specific object. In practice, the object to be protected will either be placed directly on the sensors or, if it is too small, on a small platform. These two sensors are connected to an ESP32, which communicates via LoRa with the Raspberry Pi (RPI).

4. A LED and a buzzer, which are activated when an alarm is triggered. (This is a prototype; the final product would likely use a strobe siren instead.)

5. When an intrusion occurs, a photo of the secured room and a satellite image (from the outside) are sent to a Webex room. An SMS is sent to a reference number, and an email is sent to a pre-specified address. (It could also be possible to immediately contact the police with an automated message containing the site's address. However, this is not implemented in this prototype for obvious reasons.)

In practice, a state (closed, intrusion, authenticated person) is associated with the room at all times. This state is visible on an admin dashboard, which consists of a map displaying the different secured sites, each associated with a color. In case of an intrusion, the admin has the option to deactivate the alarm. This dashboard also provides the ability to review a history of state changes for the site (stored in a database). The client also has a dashboard (limited to their site) that allows them to log in (using the RFID badge for password entry) and deactivate the alarm as needed.

Regular checks of the RPI's status will be performed using the Shodan API, which will verify the status of port 80 on the corresponding IP address. In case of failure, the admin will be notified on their dashboard.

A private VM will be used as a broker, and a public broker will be used to transmit information about site state changes to an admin (used on their dashboard and with the database) and to access login data for the database. Subscribers will be Node-RED nodes that run scripts on VMs/RPIs (components of the intrusion protocol, response to login attempts, etc.).

The proposed solution aims to encompass scenarios where the highest level of security is required, and thus includes all the above elements. However, each of these components can be seen as a "module" that can be used based on specific requirements. To achieve this, the Python scripts will be as small as possible and assembled together through the execution of multiple threads created from the scripts located in a certain directory of the file system.




## Tech implied


- Linux SysAdmin : 2 Ubuntu VM's and a Raspeberry Pi (dynamic management of the IP's, securisation, installation of miscellaneous tools from sources)
- Microcontrollers : ESP32 and ESP8266 (programation done through Arduino IDE)
- MongoDB database
- MQTT protocol (authentication, topics and securisation)
- Asymmetric encryption certificates created with openssl
- LoRaWan protocol
- Python used a scripting languages
- Node-Red
- Miscellaneous API's service used : Twilio, Teams Webex, NASA's Earth, Shodan


