#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const int pinAdc = A0;  // ESP8266 Analog Pin ADC0 = A0
const int d4Pin = 4;  

// après étalonnage   -->  valeur entre 15 et 1024   -->  intrusion à partir de 50
int sensorValue = 0;  // value read from the pot
int intrusionTreshold_soundSensor = 400;
const char *angleSensorTopic = "CGEN/angle1";
const char *soundSensorTopic = "CGEN/son1";

// wifi & mqtt mgmt
const char *ssid = "MSI Cyril"; // Enter your WiFi name
const char *password = "cgen1234";  // Enter WiFi password
//const char *mqtt_broker = "192.168.1.21";
const char *mqtt_broker = "10.43.111.221";
const char *mqtt_username = "user1";
const char *mqtt_password = "pwd1";
const int mqtt_port = 1883;
WiFiClient espClient;
//BearSSL::WiFiClientSecure espClient;
PubSubClient client(espClient);


static const char ca_cert[] PROGMEM = R"EOF(
-----BEGIN CERTIFICATE-----
MIIDmzCCAoOgAwIBAgIUBN8cLWrcoXVQlXH1jkCYvsCCE+4wDQYJKoZIhvcNAQEL
BQAwXDELMAkGA1UEBhMCQkUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDEVMBMGA1UEAwwMMTkyLjE2OC4xLjIx
MCAXDTIyMTIwNTExMDI0NloYDzIwNTAwNDIxMTEwMjQ2WjBcMQswCQYDVQQGEwJC
RTETMBEGA1UECAwKU29tZS1TdGF0ZTEhMB8GA1UECgwYSW50ZXJuZXQgV2lkZ2l0
cyBQdHkgTHRkMRUwEwYDVQQDDAwxOTIuMTY4LjEuMjEwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQChId9WD+TC85e0ok25eg2aF690ysZ8yjBcDDH4ni3f
UFY03yv3I+YHg41YFjGArNSPVl9QqifZ4kvk+PezM8kuKeESE5GR5P4BAJiEpOWT
r8EDcK114ayFz+kuAPv/iFPePVq13Q3MPEG9BG9pRVQFAleKRLIbhQZwH0qbKGm7
s27luNk8lwHgLl0q4uIUGgdAy34R25Lg2tfhBJsoSvCyJJbEN9/VcpuI3V9jS90v
EM8RIhMeiClGzXilFsLxcpaln0gZtcrV+jWWwBzj2P0JFM/TSJTJIBPKYR2dCc/2
Igh11GUkC6EmfIqHjABwHAeHL0wYzoe1/TtJPTnTUjwrAgMBAAGjUzBRMB0GA1Ud
DgQWBBShtN+CR7ATkIUXuL0JWsBduUPzRjAfBgNVHSMEGDAWgBShtN+CR7ATkIUX
uL0JWsBduUPzRjAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQBb
OBNOwb5k4uGbgDe5ht4ftiTPdm6lOpMAopJERKJO0gX6THtwublhNQQH4Arx+MxW
mOfJRQnh3abIcflqH6eP9h4zMfSFARzDKocpyfgkqAGi4fHxveciI3jYHVM+PHmY
1kk5cjyrVTlmT7wsYKhEujxBeX2COya5KruBF3kZbIWWLVJOtmXtBEoAbYRptnvh
t1ih8VOh8Cj9i6lhx22GsBnSX5UbZ73ykt6FaHlS6AP2qVMizSCocC7N0wBokpWJ
46e2N3TeY+b0oxseidxHEl5/w2jKf28Sax60E2daJWWum2zvgG4iv9lgs4W+f4k4
DQgnJm/kKJwh9TQBtunp
-----END CERTIFICATE-----
)EOF";

static const char client_cert[] PROGMEM = R"EOF(
-----BEGIN CERTIFICATE-----
MIIDOjCCAiICFHpKyqBJWysn7HJ/1225CBv8BuxhMA0GCSqGSIb3DQEBCwUAMFwx
CzAJBgNVBAYTAkJFMRMwEQYDVQQIDApTb21lLVN0YXRlMSEwHwYDVQQKDBhJbnRl
cm5ldCBXaWRnaXRzIFB0eSBMdGQxFTATBgNVBAMMDDE5Mi4xNjguMS4yMTAeFw0y
MzAxMTkwOTEyMDVaFw0yNTEwMTQwOTEyMDVaMFcxCzAJBgNVBAYTAkJFMRMwEQYD
VQQIDApTb21lLVN0YXRlMSEwHwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBM
dGQxEDAOBgNVBAMMB2VzcENlcnQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK
AoIBAQC5JzBHc/eWLiCacZy5dGZDNVwxPMczL5WHEqXdgBVA4CP6rt1NHK2f5K7P
2KG1GX3Jwqz4Qc2mCRfMWeZUOSCY/N7yeHgsm67r0MAh+G3dufVC+xoBLt1TjPxg
ZAEMrl3YJZLgxZfjm+syQ29jQLWDO/aAzFsEGQwbio9F0TIe5H3PoIS+5Bhf6PlP
r90zVZ2RBA0hwfKJ3hER3iosEGtqn4ZLbpFCXeQTiGXuOVM49k8UWsZcJ+YlCOk8
HlePxsB9nNtgYHZYzoOmMFD+8osaTScNfQwLnQeKGMheWemNkQ7z/ktGIk30RUzm
O32DOGvZPrFFnW3TaEFcNyLpVonXAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAFZN
+argKLhbOeGndjapIrf/P+DxWkqZH4WaVcQCQ25IkGnvzmOXd1BpunwNg1dn/kjm
CnOdVSbNH9VGBqCL5j/rckd+DKJjCIyfZ7RwCIrzNcEO2bz6QT+01ttrB4PtZh/n
80t/55QEbTQa6mBPxeUXH/W/RLhWZdoWfH5MCjJTht09+ywJtykoeiwqs+y3X/FM
GR/6jv24wIXEvSOR525oijvuzA7eo1+PdWOejjLJyRTIn0A1p8aYnmLqZmJvUSfN
7ADCJlk164PEmqR8Jx/HH1e4JUldKTCh2zgwiW0V/iCqvDPgDFiZ2IRvd5RPRBIt
tmb8eBgr7oWqOrGnRas=
-----END CERTIFICATE-----
)EOF";

static const char client_key[] PROGMEM = R"KEY(
-----BEGIN PRIVATE KEY-----
MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC5JzBHc/eWLiCa
cZy5dGZDNVwxPMczL5WHEqXdgBVA4CP6rt1NHK2f5K7P2KG1GX3Jwqz4Qc2mCRfM
WeZUOSCY/N7yeHgsm67r0MAh+G3dufVC+xoBLt1TjPxgZAEMrl3YJZLgxZfjm+sy
Q29jQLWDO/aAzFsEGQwbio9F0TIe5H3PoIS+5Bhf6PlPr90zVZ2RBA0hwfKJ3hER
3iosEGtqn4ZLbpFCXeQTiGXuOVM49k8UWsZcJ+YlCOk8HlePxsB9nNtgYHZYzoOm
MFD+8osaTScNfQwLnQeKGMheWemNkQ7z/ktGIk30RUzmO32DOGvZPrFFnW3TaEFc
NyLpVonXAgMBAAECggEAHbjhQq8mkRixiilvz8OUQPi5ZnwLvDdIySigsLiFcOcq
T20vwFe4tYC34Uy01MYVT8StEKM7mIpAJEFQPLf4+CYrwf+9xudtApT9qtZfCKd3
7Dsz9vmndOiegPi8xGTfUrnhZKzDj7mxXV2+7U9vc/Q1w4JKoBNHQsqQ6lPFOHkO
CJuwO99PrKuRPxkUJW1JZDY/aQwic11mwb3EJTEJeQsWsG5ofJ9TeDzZedhNCC+v
5FMBPEjlGZ/HaS6vW7kHUxCvMGuRg3JEP4hfv71Qeb2kXXb8X/iahQQUZjimiWCI
XbIAl5lsn+KZCCm7tmh21SQnQMcGTHEcLpNHnzhuWQKBgQD4eBIz61hNF/fT8LnZ
dQiEMDKwydBu1abIJtfHLMFAgwgC08WgELRm8CJxSRok7nGelvcqsrHPUzsVqA7J
GRa4GFmDCQAy5TfeeeZlbc1imHjz91Wf6j0gm9F6bjcLtlrLHN+sLjuXGOiXtVzZ
b+7897NfeY4Fyu8HvmTHwjFqKQKBgQC+w9WXnGGM0UlFhghYC+PWSnnwmZsq03nS
WZI72IeLT9KaBid6Yp0EBFpqmL3TYbPoFEujXl13bEBWvI+7X4ydQ+nzMmIHfllk
qGLnXd9/npmv7TqQhac8XNKtIqOYrBRKIxqfr+M0byuUoFvsuCp8v5+tUR7xAcJ9
dCCVUm7T/wKBgQDhdLo3qEZQdf4nrxhEbICUydNFPWrHF1Cl7zD+huq4Ke17+0Mo
sd+wRBdMqiNx/XbVi3CtqsXTNVe4l+0KppxJ4bZfQyyOAhXG5t+bi53A6E9gOD7W
w0pCK4FR0W5bT1VlVkbTSoRB35U2C5g7qOi3W9/EM10efnyUjzuNUvVZuQKBgQCH
4cLRHak2vxTsGs7yuHVv/NjjEuc+GCjcNsU9L71M9GzVHbEdMgUaCWfGKkp8uUmG
9H30PpFhMZCat/HBzAvDMc8BVlARemR+mJrqpyk+JGDguaBPtYQsMQo4HUZex6Qk
MuESAh1bCh0G3zrbYUuSVISO1guuLEy3Q/K5qmAbTQKBgQC3J9/IUzOZ4Ij4k/v3
fOAYWzSzgO543DS/cuRcsvHbEgkc0Je5EE/m/CqgE/Q4UAoAyUcn3EyDqOhALSFW
eph9coADsrex/KYb2OY0fZdeN/mac41wwkbHR6YCeXge9zMVUVWI21KrbufV3i8l
H7Wapb7L8NCzANwZsWjkYxtJsw==
-----END PRIVATE KEY-----
)KEY";

// Extracted by: openssl x509 -fingerprint -in ca.crt
static const char fp[] PROGMEM = "89:89:23:72:37:3A:D7:F3:A7:6A:FE:8C:E4:5D:41:D8:03:4A:B1:1E";
//const char* fingerprint = "46 B2 C3 44 9C 59 09 8B 01 B6 F8 BD 4C FB 00 74 91 2F EF F6";


int digitalVal;
int prevVal;



void setup() {
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(d4Pin, INPUT);
 // pinMode(analogInPin, INPUT);
  
  // connecting to a WiFi network
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    blinkWithThatDelay(100);
    Serial.println("Connecting to WiFi..");
  }
   Serial.println("!!! Connected to the WiFi network !!!");
}




void loop() {
  rotaryAngleSensorTest();
  soundSensorTest();
  blinkWithThatDelay(1000); 
}



void rotaryAngleSensorTest() {  // un seul port analogique, et pas de convertisseur ADC  -->  l'étonnage montre une HIGH pour un angle de 25-30°
  sensorValue = digitalRead(d4Pin);
  //Serial.print("d read  = " +  String(sensorValue) + "\n");
  if (sensorValue == 1){
    Serial.print("send :  DOOR1 OPENED !!!\n");
    connectToBrokerAndSendMessage(angleSensorTopic, "DOOR1 OPENED !!!");
  }
}

void soundSensorTest(){
    long sum = 0;

    for(int i=0; i<1024; i++)
    {
        sum += analogRead(pinAdc);
    }
    
    sum >>= 10;
    Serial.print("\nsound sensor : ");
   Serial.print(sum);
    if (sum > intrusionTreshold_soundSensor){
      Serial.print("   -->   send :  SOUND SENSOR ACTIVATED !!!\n");
      connectToBrokerAndSendMessage(soundSensorTopic, "SOUND SENSOR ACTIVATED !!!");
    }
}

void blinkWithThatDelay(int milli){

  digitalWrite(LED_BUILTIN, HIGH);   
  delay(milli);                     
  digitalWrite(LED_BUILTIN, LOW);    
  delay(milli); 

}


void connectToBrokerAndSendMessage(const char* topic, const char* msg){

//  BearSSL::X509List cert(ca_cert);
//  espClient.setTrustAnchors(&cert);  
//  BearSSL::X509List client_crt(client_cert);
//  BearSSL::PrivateKey key(client_key);
//  espClient.setClientRSACert(&client_crt, &key);
//    //espClient.setFingerprint(fp);
//  espClient.setInsecure();
  
  client.setServer(mqtt_broker, mqtt_port);
  while (!client.connected()) {
   String client_id = "CGEN-esp8266-client-";
      client_id += String(WiFi.macAddress());
      Serial.printf("The client %s connects to the public mqtt broker\n", client_id.c_str());
      if (client.connect(client_id.c_str(), mqtt_username, mqtt_password)) {
          Serial.println("Public emqx mqtt broker connected");
      } else {
          Serial.print("failed with state " + String(client.state()) + "\n" );
          blinkWithThatDelay(200); 
          delay(4000);
      }                    
   }
  

  client.publish(topic, msg);
  
}
