#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <MQTT.h>
#include <time.h>

//enable only one of these below, disabling both is fine too.
// #define CHECK_CA_ROOT
// #define CHECK_PUB_KEY
#define CHECK_FINGERPRINT
////--------------------------////



#ifndef SECRET
    const char ssid[] = "Proximus-Home-E5B0"; // Enter your WiFi name
    const char pass[] = "ws64xfah727rf";  // Enter WiFi password

    #define HOSTNAME "CGEN-esp8266-client"

    const char MQTT_HOST[] = "192.168.1.21";
    const int MQTT_PORT = 8883;
    const char MQTT_USER[] = "user1";
    const char MQTT_PASS[] = "pwd1";

    const char MQTT_SUB_TOPIC[] = "home/" HOSTNAME "/in";
    const char MQTT_PUB_TOPIC[] = "CGEN/angle1";

    #ifdef CHECK_CA_ROOT
    static const char digicert[] PROGMEM = R"EOF(
    -----BEGIN CERTIFICATE----- MIIDOjCCAiICFHpKyqBJWysn7HJ/1225CBv8BuxhMA0GCSqGSIb3DQEBCwUAMFwx CzAJBgNVBAYTAkJFMRMwEQYDVQQIDApTb21lLVN0YXRlMSEwHwYDVQQKDBhJbnRl cm5ldCBXaWRnaXRzIFB0eSBMdGQxFTATBgNVBAMMDDE5Mi4xNjguMS4yMTAeFw0y MzAxMTkwOTEyMDVaFw0yNTEwMTQwOTEyMDVaMFcxCzAJBgNVBAYTAkJFMRMwEQYD VQQIDApTb21lLVN0YXRlMSEwHwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBM dGQxEDAOBgNVBAMMB2VzcENlcnQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK AoIBAQC5JzBHc/eWLiCacZy5dGZDNVwxPMczL5WHEqXdgBVA4CP6rt1NHK2f5K7P 2KG1GX3Jwqz4Qc2mCRfMWeZUOSCY/N7yeHgsm67r0MAh+G3dufVC+xoBLt1TjPxg ZAEMrl3YJZLgxZfjm+syQ29jQLWDO/aAzFsEGQwbio9F0TIe5H3PoIS+5Bhf6PlP r90zVZ2RBA0hwfKJ3hER3iosEGtqn4ZLbpFCXeQTiGXuOVM49k8UWsZcJ+YlCOk8 HlePxsB9nNtgYHZYzoOmMFD+8osaTScNfQwLnQeKGMheWemNkQ7z/ktGIk30RUzm O32DOGvZPrFFnW3TaEFcNyLpVonXAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAFZN +argKLhbOeGndjapIrf/P+DxWkqZH4WaVcQCQ25IkGnvzmOXd1BpunwNg1dn/kjm CnOdVSbNH9VGBqCL5j/rckd+DKJjCIyfZ7RwCIrzNcEO2bz6QT+01ttrB4PtZh/n 80t/55QEbTQa6mBPxeUXH/W/RLhWZdoWfH5MCjJTht09+ywJtykoeiwqs+y3X/FM GR/6jv24wIXEvSOR525oijvuzA7eo1+PdWOejjLJyRTIn0A1p8aYnmLqZmJvUSfN 7ADCJlk164PEmqR8Jx/HH1e4JUldKTCh2zgwiW0V/iCqvDPgDFiZ2IRvd5RPRBIt tmb8eBgr7oWqOrGnRas= -----END CERTIFICATE-----
    )EOF";
    #endif

    #ifdef CHECK_PUB_KEY
    // Extracted by: openssl x509 -pubkey -noout -in ca.crt
    static const char pubkey[] PROGMEM = R"KEY(
    -----BEGIN PUBLIC KEY----- MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoSHfVg/kwvOXtKJNuXoN mhevdMrGfMowXAwx+J4t31BWNN8r9yPmB4ONWBYxgKzUj1ZfUKon2eJL5Pj3szPJ LinhEhORkeT+AQCYhKTlk6/BA3CtdeGshc/pLgD7/4hT3j1atd0NzDxBvQRvaUVU BQJXikSyG4UGcB9Kmyhpu7Nu5bjZPJcB4C5dKuLiFBoHQMt+EduS4NrX4QSbKErw siSWxDff1XKbiN1fY0vdLxDPESITHogpRs14pRbC8XKWpZ9IGbXK1fo1lsAc49j9 CRTP00iUySATymEdnQnP9iIIddRlJAuhJnyKh4wAcBwHhy9MGM6Htf07ST0501I8 KwIDAQAB -----END PUBLIC KEY-----
    )KEY";
    #endif

    #ifdef CHECK_FINGERPRINT
  // Extracted by: openssl x509 -fingerprint -in ca.crt
    static const char fp[] PROGMEM = "89:89:23:72:37:3A:D7:F3:A7:6A:FE:8C:E4:5D:41:D8:03:4A:B1:1E";
    #endif
#endif
//////////////////////////////////////////////////////

#if (defined(CHECK_PUB_KEY) and defined(CHECK_CA_ROOT)) or (defined(CHECK_PUB_KEY) and defined(CHECK_FINGERPRINT)) or (defined(CHECK_FINGERPRINT) and defined(CHECK_CA_ROOT)) or (defined(CHECK_PUB_KEY) and defined(CHECK_CA_ROOT) and defined(CHECK_FINGERPRINT))
#error "cant have both CHECK_CA_ROOT and CHECK_PUB_KEY enabled"
#endif

BearSSL::WiFiClientSecure net;
MQTTClient client;

unsigned long lastMillis = 0;
time_t now;

void mqtt_connect()
{
    Serial.print("checking wifi...");
    while (WiFi.status() != WL_CONNECTED)
    {
        Serial.print(".");
        delay(1000);
    }

    Serial.print("\nMQTT connecting ");
    String client_id = "CGEN-esp8266-client-";
    client_id += String(WiFi.macAddress());
    while (!client.connect(HOSTNAME, MQTT_USER, MQTT_PASS))
    {
        Serial.print("\n");
        Serial.print(HOSTNAME);
        Serial.print(" ");
        Serial.print(MQTT_USER);
        Serial.print(" ");
        Serial.print(MQTT_PASS);
        delay(1000);
    }

    Serial.println("connected!");

    client.subscribe(MQTT_SUB_TOPIC);
}

void messageReceived(String &topic, String &payload)
{
    Serial.println("Recieved [" + topic + "]: " + payload);
}

void setup()
{
    Serial.begin(115200);
    Serial.println();
    Serial.println();
    Serial.print("Attempting to connect to SSID: ");
    Serial.print(ssid);
    WiFi.hostname(HOSTNAME);
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, pass);
    while (WiFi.status() != WL_CONNECTED)
    {
        Serial.print(".");
        delay(1000);
    }
    Serial.println("connected!");

    Serial.print("Setting time using SNTP");
    configTime(-5 * 3600, 0, "pool.ntp.org", "time.nist.gov");
    now = time(nullptr);
    while (now < 1510592825)
    {
        delay(500);
        Serial.print(".");
        now = time(nullptr);
    }
    Serial.println("done!");
    struct tm timeinfo;
    gmtime_r(&now, &timeinfo);
    Serial.print("Current time: ");
    Serial.print(asctime(&timeinfo));


    client.begin(MQTT_HOST, MQTT_PORT, net);
    client.onMessage(messageReceived);

    Serial.print("\nwill start mqtt_connects !!!! \n");

    mqtt_connect();

    Serial.print("\nSET UP DONE !!!! \n");
}

void loop()
{
    now = time(nullptr);
    if (WiFi.status() != WL_CONNECTED)
    {
        Serial.print("Checking wifi");
        while (WiFi.waitForConnectResult() != WL_CONNECTED)
        {
            WiFi.begin(ssid, pass);
            Serial.print(".");
            delay(10);
        }
        Serial.println("connected");
    }
    else
    {
        if (!client.connected())
        {
            mqtt_connect();
        }
        else
        {
            client.loop();
        }
    }

    // publish a message roughly every second.
    if (millis() - lastMillis > 5000)
    {
        lastMillis = millis();
        client.publish(MQTT_PUB_TOPIC, ctime(&now), false, 0);
    }
}
