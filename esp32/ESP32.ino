#include <SimpleDHT.h>
#include "Ultrasonic.h"
#include <LoRa.h>
#include <SPI.h>

const int pin16 = 16;
const int pin13 = 13;

// variable for storing the potentiometer value
int potValue16 = 0;
int potValue13 = 0;

Ultrasonic ultrasonic(13);
long RangeInInches;
long RangeInCentimeters;

#define SCK 5
#define MISO 19
#define MOSI 27
#define SS 18
#define RST 14
#define DIO0 26
#define BAND 866E6  //  433E6 for Asia  -  866E6 for Europe  -  915E6 for North America



void setup(){
  pinMode(pin16, INPUT);
  pinMode(pin13, INPUT);
  Serial.begin(115200);
  delay(1000);
  
  //SPI LoRa pins
  SPI.begin(SCK, MISO, MOSI, SS);
  //setup LoRa transceiver module
  LoRa.setPins(SS, RST, DIO0);
  
  if (!LoRa.begin(BAND)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
  Serial.println("LoRa Initializing OK!");
  delay(2000);
}



void loop() {
  potValue16 = digitalRead(pin16);
  if (potValue16 == 1){
    intrusion("Bouton 1");
  }
  
  delay(1000);
  
  RangeInCentimeters = ultrasonic.MeasureInCentimeters(); // two measurements should keep an interval
  if (RangeInCentimeters > 1 && RangeInCentimeters != 528){  // don't know why, but 528cm is rea when something is directly covering the sensor
    Serial.println("range read in cm : ");
    Serial.println(RangeInCentimeters);
    intrusion("Ultrasonic Sensor 1");
  }
  
  delay(1000);

  Serial.print("Sending packet... ");
  
  //Send LoRa packet to receiver
  LoRa.beginPacket();
  LoRa.print("hello ");
  LoRa.endPacket();
}


void intrusion(const char* msg){
    Serial.println(msg);
}
