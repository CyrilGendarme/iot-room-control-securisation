import sys
sys.path.insert(0, "/home/pi/Desktop/iot/scripts/others_scripts")
from testMQTT import *

import paho.mqtt.client as paho 
import time 

broker_ip = "192.168.137.21" 
broker_port = 1883
username="user1"
pwd="pwd1"
mqtt_topics = ['CGEN/angle1','CGEN/son1']

def on_message(client, userdata, message):
    print("message received: " + str(message.payload))
    print("debug 1")
    mqtt_pub('{"message":"intrusion","site":"site1"}', "CGEN/site1/intrusion", "127.0.0.1")
    print("debug 2")
    
def mqtt_sub(mqtt_topics = []):
    client=paho.Client("CGEN_clientSon") # client1 = clientID
    client.username_pw_set(username, pwd)
    #client.tls_set("/home/pi/Desktop/tempCertsCreation/ca.crt",
     #              "/home/pi/Desktop/tempCertsCreation/client.crt",
     #              "/home/pi/Desktop/tempCertsCreation/client.key",
    #             tls_version=2)
    #client.tls_insecure_set(True)
    client.on_message=on_message
    client.connect(broker_ip, broker_port, 60) # 60 is keepalive
    print("Connected to broker")
    for topic in mqtt_topics:
        client.subscribe(topic)#optionnel : client.subscribe(mqtt_topic, QoS)
        print("subscribed to topic : " + topic)
    client.loop_forever()


#mqtt_sub(mqtt_topics)
