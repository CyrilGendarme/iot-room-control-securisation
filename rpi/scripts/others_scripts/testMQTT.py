import paho.mqtt.client as paho
import time

broker_ip = "test.mosquitto.org"
broker_port = 1883
mqtt_topic = "CGEN/site1/intrusion"
payload = "{'message':'Hello World'}"

def mqtt_pub(payload, mqtt_topic, broker_ip):
	client=paho.Client("CGEN_pub1")
	client.username_pw_set(username="user2", password="CGEN2")
	client.on_publish=on_publish
	client.on_log=on_log
	client.loop_start()
	client.connect(broker_ip,broker_port,60) # 60 is keepalive
	time.sleep(1)
	client.publish(mqtt_topic, payload, 2)
	time.sleep(1) # permet au ACK de revenir
	print("message publie : " + payload)
	client.disconnect()
	client.loop_stop()

def on_publish(client, userdata, mid):
	print("message published")

def on_log(client, userdata, level, buf):
	print("log: ", buf)

if __name__ == '__main__':
	mqtt_pub(payload, mqtt_topic, broker_ip)
