import sys
sys.path.insert(0, "/home/pi/Desktop/iot/scripts/others_scripts")

import serial
import time
from testMQTT import *

ser=serial.Serial('/dev/ttyS0', baudrate=9600, timeout=1)
ser.flush() 
card=""
tempcard=""
resultcard=""
# print("RFID script launched")



def readRFID():
    global card
    global ser
    c= ser.read()
    card = card+c 
    if len(card)==14:
        card=card[5:11] 
        tempcard=card
        mqtt_pub('{"rfid":"'+card+'","site":"site1"}', "CGEN/site1/rfid", "127.0.0.1")
        card=""
        return tempcard


while True:
    resultcard=readRFID()
    if (resultcard):
        print(resultcard)