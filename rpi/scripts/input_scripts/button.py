import sys
sys.path.insert(0, "/home/pi/Desktop/iot/scripts/output_scripts")

from lcdScreen_I2C_1 import unauthentifiedMessage, clearScreen

import time
from grovepi import *
from testMQTT import *


button = 7
pinMode(button,"INPUT")
time.sleep(0.1)

while True:
    try:
        for i in range(6):
            #print("... " + str(i))
            if (digitalRead(button) == 1):
                print("push on button " + str(i))
                time.sleep(0.02)
            else :
                break;
        if (i >=5):
            mqtt_pub('{"buttonPushed":true,"site":"site1"}', "CGEN/site1/button", "127.0.0.1")
            unauthentifiedMessage()
            clearScreen()
            time.sleep(2)
                

    except IOError:
        print ("Error")