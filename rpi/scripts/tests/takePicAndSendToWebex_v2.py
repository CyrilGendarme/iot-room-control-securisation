import requests
from requests_toolbelt.multipart.encoder import MultipartEncoder

WBX_KEY = "MzhhYWY1ZjctYWQ5Ny00YzczLWJlNDYtOTViYmY4MWNlNmVmZTc4OTgyZTItYTY3_PE93_74cc8408-7643-4e09-b2b1-682def4bf071"
WBX_ROOM_ID = "Y2lzY29zcGFyazovL3VybjpURUFNOmV1LWNlbnRyYWwtMV9rL1JPT00vYmIxNTkwNTAtM2IzOS0xMWVjLThiOTgtNmIyYWEzODkzNDNl"
IMAGEPATH = "../../Images/test.jpg"

m = MultipartEncoder({'roomId': WBX_ROOM_ID,
                      'text': 'INTRUSION MESSAGE !!!',
                      'files': (IMAGEPATH, open(IMAGEPATH, 'rb'),
                       'image/jpg')})

r = requests.post('https://webexapis.com/v1/messages', data=m,
                  headers={'Authorization': 'Bearer '+WBX_KEY,
                  'Content-Type': m.content_type})

print(r.text)