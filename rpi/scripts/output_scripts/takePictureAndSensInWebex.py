import time
import requests
from requests_toolbelt.multipart.encoder import MultipartEncoder
#from urllib.request import urlretrieve
from pprint import PrettyPrinter
from datetime import datetime
from picamera import PiCamera

WBX_KEY = "MjAwNGYzZDYtM2UzYS00Zjg4LThiOTAtZjRhZDA5NDA1MGE0M2M5Y2ZjNmMtNmIy_PE93_74cc8408-7643-4e09-b2b1-682def4bf071"
WBX_ROOM_ID = "Y2lzY29zcGFyazovL3VybjpURUFNOmV1LWNlbnRyYWwtMV9rL1JPT00vYmIxNTkwNTAtM2IzOS0xMWVjLThiOTgtNmIyYWEzODkzNDNl"
IMAGEPATH = "/home/pi/Desktop/iot/Images/intrusionRoom.jpg"
IMAGEPATH2 = "/home/pi/Desktop/iot/Images/test.jpg"
URL_APOD = "https://api.nasa.gov/planetary/apod"
nasaApiKey = "J4MDB5DdIdLhDpjA0CzYPcKhkFcuf90q7GNXE2os"


def takePictureAndSensInWebex():
    pp = PrettyPrinter()


    #we took a picture
    camera = PiCamera()
    time.sleep(2)
    camera.capture(IMAGEPATH)
    print("Picture taken.")


    #we download image from api url response
    date = datetime.today().strftime('%Y-%m-%d')
    params = {
      'api_key':nasaApiKey,
      'date':date,
      'hd':'True'
    }
    response = requests.get(URL_APOD,params=params).json()
    img_data = requests.get(response['hdurl']).content
    with open(IMAGEPATH2, 'wb') as handler:
        handler.write(img_data)



    #we send the two images to webex room

    m = MultipartEncoder({'roomId': WBX_ROOM_ID,
                          'text': 'INTRUSION !!!   -->  ROOM VIEW',
                          'files': (IMAGEPATH, open(IMAGEPATH, 'rb'),
                           'image/jpg')})
    r = requests.post('https://webexapis.com/v1/messages', data=m,
                      headers={'Authorization': 'Bearer '+WBX_KEY,
                      'Content-Type': m.content_type})
    print(r.text)

    m = MultipartEncoder({'roomId': WBX_ROOM_ID,
                          'text': 'INTRUSION !!!   -->  SKY VIEW',
                          'files': (IMAGEPATH2, open(IMAGEPATH2, 'rb'),
                           'image/jpg')})
    r = requests.post('https://webexapis.com/v1/messages', data=m,
                      headers={'Authorization': 'Bearer '+WBX_KEY,
                      'Content-Type': m.content_type})
    print(r.text)
