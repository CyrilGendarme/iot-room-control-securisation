import time
from grovepi import *


led = 4
pinMode(led,"OUTPUT")
time.sleep(0.1)

print ("D{} port connected (output mode)".format(led))

def blinkOnce():
    digitalWrite(led,1)		# Send HIGH to switch on LED
    time.sleep(0.3)

    digitalWrite(led,0)		# Send LOW to switch off LED
    time.sleep(0.3)
    

# while True:
#     try:
#         blinkOnce()
#     except KeyboardInterrupt:	# Turn LED off before stopping
#         digitalWrite(led,0)
#         break
