import time
from grove_rgb_lcd import *


def printMessage(message,R,G,B, duration):
    try:
        setText(message)
        setRGB(R,G,B)
        time.sleep(duration)
    except Exception:
            pass

def clearScreen():
    try:
        printMessage("",0,0,0,0)
    except Exception:
            pass
        
def welcomeMessage():
    try:
        printMessage("Welcome !",0,255,0,4)
    except Exception:
            pass
        
def rejectMessage():
    try:
        printMessage("Forbidden !",2555,0,0,4)
    except Exception:
            pass
        
def unauthentifiedMessage():
    try:
        printMessage("Unauthentified",2555,0,0,4)
    except Exception:
            pass

#welcomeMessage()
#time.sleep(2)
#clearScreen()
#rejectMessage()
#time.sleep(2)
#clearScreen()
