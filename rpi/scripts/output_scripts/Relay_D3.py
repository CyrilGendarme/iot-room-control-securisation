import time
from grovepi import *

Relay = 3
Button = 2
pinMode(Relay,"OUTPUT")
pinMode(Button,"INPUT")
time.sleep(1)
hasToDring=False

print ("D{} port connected (output mode)".format(Relay))
print ("D{} port connected (input mode)".format(Button))

while True:
        try:
            if (digitalRead(Button)==1) :
                digitalWrite(Relay,1)
                print("on")
            else :
                digitalWrite(Relay, 0)
                print("off")
            time.sleep(5)
        except KeyboardInterrupt:
            digitalWrite(Relay,0)
            break
        except IOError:
            print("IOError")
